Based on the [UnityEvent](http://docs.unity3d.com/Manual/UnityEvents.html) class, QuickEvents allow you to 
assign persistent event handlers through the inspector, passing either 
static or dynamic values to fields, properties and methods.

Features include:

* Reorderable list of event handlers
* Support for targeting a component of which there are multiple instances on a single game object
* Pass multiple arguments using the AdvancedEvent class
* Control over which types of members are listed using the EventFilter attribute

Asset Store: [http://u3d.as/nAa](http://u3d.as/nAa)